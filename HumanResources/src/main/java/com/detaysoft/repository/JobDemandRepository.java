package com.detaysoft.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.model.JobDemand;

public interface JobDemandRepository extends CrudRepository<JobDemand, Long> {
	List<JobDemand> findAll();
	List<JobDemand> findByActive(Boolean active);
	JobDemand findById(long id);
	
	
}