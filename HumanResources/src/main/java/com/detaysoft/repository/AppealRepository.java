package com.detaysoft.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.model.Appeal;

public interface AppealRepository extends CrudRepository<Appeal, Long> {
	 List<Appeal> findAll();

}