package com.detaysoft.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

	User findByEmailEquals(String email);

}