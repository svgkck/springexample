package com.detaysoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.detaysoft.service.UserService;

@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/user/add", method = RequestMethod.GET)
	public void add(@RequestParam(value = "email", required = true) String username,
			@RequestParam(value = "password", required = true) String password) {
		
		 userService.add(username,password);
	}
}
