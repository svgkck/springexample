package com.detaysoft.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.detaysoft.model.JobDemand;
import com.detaysoft.service.JobDemandService;

@RestController
public class JobDemandController {

	@Autowired
	private JobDemandService jobDemandService;

	@RequestMapping(value = "/jobdemand/active", method = RequestMethod.GET)
	public List<JobDemand> getActive() {
		return jobDemandService.findActive();
	}

	@RequestMapping(value = "/jobdemand/all", method = RequestMethod.GET)
	public List<JobDemand> getAll() {
		JobDemand jobDemand= new JobDemand();
		jobDemand.setActive(false);
		jobDemand.setAppeal(null);
		jobDemand.setClosingDate(new Date());
		jobDemand.setCreatedDate(new Date());
		jobDemand.setDemandDefinition("aasssaas");
		jobDemand.setPositionName("poz1");
		jobDemandService.add(jobDemand);
		return jobDemandService.findAll();
	}

	@RequestMapping(value = "/jobdemand/add", method = RequestMethod.PUT)
	public void add(@RequestBody JobDemand jobDemand) {
		jobDemandService.add(jobDemand);
	}
	

}
