package com.detaysoft.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.detaysoft.model.Appeal;
import com.detaysoft.service.AppealService;
import com.detaysoft.service.JobDemandService;


@RestController
public class AppealController {
	@Autowired
	private AppealService appealService;
	@Autowired
	private JobDemandService jobDemand;

	@RequestMapping(value = "/appeal/all", method = RequestMethod.GET)
	public List<Appeal> getActive() {
		Appeal appeal= new Appeal();
		appeal.setName("");
		appeal.setSurname("");
		appeal.setPhone("111111111");
		appeal.setEmail("svg@svg.com");
		appeal.setAppealResult("aaaa");
		appeal.setInterview(true);
		appeal.setAppealDate(new Date());
		appeal.setJobDemand(jobDemand.findAll().get(0));
		
		appealService.add(appeal);
		return appealService.findAll();
	}
}
