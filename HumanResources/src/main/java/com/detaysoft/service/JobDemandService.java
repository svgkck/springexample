package com.detaysoft.service;

import java.util.List;

import com.detaysoft.model.JobDemand;

public interface JobDemandService {
	
	void add(JobDemand jobDemand);
	void deleteJobDemand();
	JobDemand get(long id);
	List<JobDemand> findAll();
	List<JobDemand> findActive();
	
}
