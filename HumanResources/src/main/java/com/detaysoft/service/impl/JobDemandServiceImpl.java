package com.detaysoft.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.detaysoft.model.JobDemand;
import com.detaysoft.repository.JobDemandRepository;
import com.detaysoft.service.JobDemandService;

@Service
public class JobDemandServiceImpl implements JobDemandService {

	@Autowired
	private JobDemandRepository jobDemandRepo;

	@Override
	public void add(JobDemand jobDemand) {
		// TODO Auto-generated method stub

		jobDemandRepo.save(jobDemand);
	}

	@Override
	public List<JobDemand> findAll() {
		// TODO Auto-generated method stub
		return jobDemandRepo.findAll();
	}

	@Override
	public List<JobDemand> findActive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JobDemand get(long id) {
		// TODO Auto-generated method stub
		return jobDemandRepo.findById(id);
	}

	@Override
	public void deleteJobDemand() {
		// TODO Auto-generated method stub
		List<JobDemand> job = jobDemandRepo.findAll();
		for (int i = 0; i < job.size(); i++) {
			if (job.get(i).getActive() == false) {
				jobDemandRepo.delete(job.get(i));
			}
		}
	}

}
