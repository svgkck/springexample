package com.detaysoft.service.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.detaysoft.model.User;
import com.detaysoft.repository.UserRepository;
import com.detaysoft.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepo;

	@Override
	public String  add(String email, String password) {
		User user=new User();
		user.setEmail(email);
		
		MessageDigest messageDigestNesnesi;
		try {
			messageDigestNesnesi = MessageDigest.getInstance("MD5");
			messageDigestNesnesi.update(password.getBytes());
			byte messageDigestDizisi[] = messageDigestNesnesi.digest();
			StringBuffer sb32 = new StringBuffer();
			for (int i = 0; i < messageDigestDizisi.length; i++) {
				sb32.append(Integer.toString((messageDigestDizisi[i] & 0xff) + 0x100, 32));
			}

			user.setPassword(sb32.toString());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		userRepo.save(user);
		return user.getEmail();
	}

	@Override
	public boolean authenticate(String username, String password) {
		User user = userRepo.findByEmailEquals(username);
		MessageDigest messageDigestNesnesi;
		try {
			messageDigestNesnesi = MessageDigest.getInstance("MD5");
			messageDigestNesnesi.update(password.getBytes());
			byte messageDigestDizisi[] = messageDigestNesnesi.digest();
			StringBuffer sb32 = new StringBuffer();
			for (int i = 0; i < messageDigestDizisi.length; i++) {
				sb32.append(Integer.toString((messageDigestDizisi[i] & 0xff) + 0x100, 32));
			}
			
				if (user.getPassword().equals(sb32.toString())) {
					return true;
				}
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return false;
	}

}
