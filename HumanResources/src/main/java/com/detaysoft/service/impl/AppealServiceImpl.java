package com.detaysoft.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.detaysoft.model.Appeal;
import com.detaysoft.repository.AppealRepository;
import com.detaysoft.service.AppealService;


@Service
public class AppealServiceImpl implements AppealService {

	@Autowired
	private AppealRepository appealRepository;

	@Override
	public void add(Appeal appeal) {
		// TODO Auto-generated method stub
		appealRepository.save(appeal);
	}

	@Override
	public List<Appeal> findAll() {
		// TODO Auto-generated method stub
		return appealRepository.findAll();
	}

}
