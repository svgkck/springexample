package com.detaysoft.service;

import java.util.List;

import com.detaysoft.model.Appeal;

public interface AppealService {

	void add(Appeal appeal);
	
	List<Appeal> findAll();
}
