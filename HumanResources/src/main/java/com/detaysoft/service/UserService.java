package com.detaysoft.service;

import com.detaysoft.model.User;

public interface UserService {
	String add(String email, String Password);
	boolean authenticate(String username, String password);
}
