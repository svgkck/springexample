package com.detaysoft.model;
//- id

//- İsim
//- Soyisim
//- email
//- telefon
//- Başvuru tarihi
//- Görüşme Yapıldı(boolean)
//- Başvuru sonucu
//- İş Talebi(ManyToOne

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity(name = "appeals")
public class Appeal {

	@Id
	@Column(name = "appeal_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String surname;
	private String email;
	private String phone;
	private Date appealDate;
	private boolean isInterview;
	private String appealResult;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "jobdemand_id")
	private JobDemand jobDemand;

	public Appeal() {

	}

	public Appeal(String name, String surname, String email, String phone, Date appealDate, boolean isInterview,
			String appealResult, JobDemand jobDemand) {
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.phone = phone;
		this.appealDate = appealDate;
		this.isInterview = isInterview;
		this.appealResult = appealResult;
		this.jobDemand = jobDemand;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getAppealDate() {
		return appealDate;
	}

	public void setAppealDate(Date appealDate) {
		this.appealDate = appealDate;
	}

	public boolean isInterview() {
		return isInterview;
	}

	public void setInterview(boolean isInterview) {
		this.isInterview = isInterview;
	}

	public String getAppealResult() {
		return appealResult;
	}

	public void setAppealResult(String appealResult) {
		this.appealResult = appealResult;
	}

	public JobDemand getJobDemand() {
		return jobDemand;
	}

	public void setJobDemand(JobDemand jobDemand) {
		this.jobDemand = jobDemand;
	}

}