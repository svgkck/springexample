package com.detaysoft.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "jobdemand")
public class JobDemand {

	@Id
	@Column(name = "jobdemand_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String demandDefinition;
	private Date createdDate;
	private Date closingDate;
	private Boolean active;
	private String positionName;

	@JsonManagedReference
	@OneToMany(mappedBy = "jobDemand", cascade = CascadeType.ALL)
	private List<Appeal> appeal;

	public JobDemand() {

	}

	public JobDemand(String demandDefinition, Date createdDate, Date closingDate, Boolean active, String positionName,
			List<Appeal> appeal) {
		super();
		this.demandDefinition = demandDefinition;
		this.createdDate = createdDate;
		this.closingDate = closingDate;
		this.active = active;
		this.positionName = positionName;
		this.appeal = appeal;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDemandDefinition() {
		return demandDefinition;
	}

	public void setDemandDefinition(String demandDefinition) {
		this.demandDefinition = demandDefinition;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getClosingDate() {
		return closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public List<Appeal> getAppeal() {
		return appeal;
	}

	public void setAppeal(List<Appeal> appeal) {
		this.appeal = appeal;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
