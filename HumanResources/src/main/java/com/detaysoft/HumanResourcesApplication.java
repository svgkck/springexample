package com.detaysoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.detaysoft.service.UserService;

@SpringBootApplication
public class HumanResourcesApplication {
	@Autowired
	UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(HumanResourcesApplication.class, args);
	}

//	@Bean
//	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//		return args -> {
//			userService.add("svg", "12345");
//			userService.add("denden", "12345");
//		};
//	}
}
