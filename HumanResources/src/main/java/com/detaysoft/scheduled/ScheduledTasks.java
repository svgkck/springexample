package com.detaysoft.scheduled;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.detaysoft.model.JobDemand;
import com.detaysoft.service.JobDemandService;

@Component
public class ScheduledTasks {
	@Autowired
	JobDemandService jobDemandService;

	@Scheduled(fixedRate = 1000)
	public void reportCurrentTime() {
		jobDemandService.deleteJobDemand();
	}
}